New home for this project:

https://codeberg.org/daniel-casanueva/matrix

# matrix package

Haskell Matrix library with common operations with them.

You can find usage examples in the documentation.

# Looking for new maintainer

If you are interested in maintaining and/or developing `matrix`,
please get in touch. The library will continue to work, but
no one is currently working on new features or improvements.
Merge requests are also accepted. Thanks.