## 0.3.6.3
* Metadata update.

## 0.3.6.2
* Fixes for `rref` and `fromList`.
